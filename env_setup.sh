#/bin/sh
#Maintainer Rohit Sureka
clear


echo "
Please select :
	0. Quit
	1. Setting up web app env for system (linux )
	2. Setting up web app env for system (Mac)
"
read -p "Enter selection [0-2] > "

if [[ $REPLY =~ ^[0-3]$ ]]; then
	
	if [[ $REPLY == 0 ]]; then
		echo "Terminating ..."
		exit
	fi

	if [[ $REPLY == 1 ]]; then
		if ! node_loc="$(type -p "node")" || [[ -z $node_loc ]]; then
			echo   "installing node ....." 
			sudo apt-get update -yq
			sudo apt-get install nodejs -yq
			sudo apt-get install npm -yq
		else 
			echo "node is already available "
		fi 
		if ! mongodb_loc="$(type -p "mongod")" || [[ -z $mongodb_loc ]]; then
  			echo   "installing mongodb ....."
  			sudo apt-get update -yq
  			sudo apt-get install -y mongodb

		else
			echo "mongodb is already available "
		fi	
        exit
    fi

    if [[ $REPLY == 2 ]]; then
    	if test ! $(which brew); then
 		   echo "Installing homebrew..."
    	   ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
		fi

		if test ! $(which node); then
			echo   "installing node  ....." 
			brew update -y
			brew install node -y			
		else 
			echo "node is already available "
		fi 
		if test ! $(which mongodb); then
  			echo "installing mongodb for api server   ....."
  			brew update -y
  			brew install mongodb
		else
			echo "mongodb is already available "
		fi	
        exit
    fi



else
    echo "Invalid entry." >&2
    exit 1
fi
